package com.zhr.controller;

import com.zhr.common.ResultData;
import com.zhr.common.ResultState;
import com.zhr.tools.Properties2Yml;
import com.zhr.tools.Yml2Properties;
import com.zhr.tools.code3.entityParameter.YmlInfo;
import org.springframework.web.bind.annotation.*;

/**
 * @author zhr
 */
@RestController
public class YmlController {
    /**
     * properties to yml
     */
    private static final int TYPE_TO_YML = 1;
    /**
     * yml to properties
     */
    private static final int TYPE_TO_PROPERTIES = 2;

    /**
     * properties yml 装换
     *
     * type    1 p->y 2 y->p
     * content 待加转换内容
     * @return 转换结果
     */
    @PostMapping("yml")
    public ResultData<String> sendRequest(
            @RequestBody YmlInfo ymlInfo) {
        try {
            if (ymlInfo.getType() == TYPE_TO_YML) {
                return new ResultData<>(Properties2Yml.convert(ymlInfo.getContent()));
            } else if (ymlInfo.getType() == TYPE_TO_PROPERTIES) {
                return new ResultData<>(Yml2Properties.convert(ymlInfo.getContent()));
            } else {
                return new ResultData<>(ResultState.BIZ_FAIL, "请输入正确操作类型");
            }
        } catch (Exception e) {
            return new ResultData<>(e.getMessage() + "\n");
        }
    }
}
