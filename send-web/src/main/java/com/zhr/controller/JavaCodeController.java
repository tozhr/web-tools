package com.zhr.controller;

import com.zhr.common.ResultData;
import com.zhr.tools.code3.entityParameter.JavaCodeInfo;
import org.springframework.web.bind.annotation.*;

import static com.zhr.tools.compile.GenGetSetTools.gen;

/**
 * @author ZHR
 */
@RestController
public class JavaCodeController {
    /**
     * JavaClassCode to java code
     *
     *  javaCode      Java代码
     *  methodPrefix  类的方法前缀
     *  genCodePrefix 生成的代码 方法名之前
     *  genCodeSuffix 生成的代码 方法名之后
     * @return 代码
     */
    @PostMapping("java-method")
    public ResultData<String> genJavaCode(
             @RequestBody JavaCodeInfo javaCodeInfo
            ) {
        try {
            return new ResultData<>(gen(javaCodeInfo.getJavaCode(), javaCodeInfo.getMethodPrefix(), javaCodeInfo.getGenCodePrefix(), javaCodeInfo.getGenCodeSuffix()));
        } catch (Exception e) {
            return new ResultData<>(e.getMessage() + "\n");
        }
    }

    public static void main(String[] args) throws Exception {
        String javaCode="public class Test{\n" +
                "private String name;\n" +
                "public String getName() {\n" +
                "  return name;\n" +
                "}\n" +
                "public void setName(String name) {\n" +
                "this.name = name;\n" +
                "}\n" +
                "}\n";
        String set = gen(javaCode, "set", "obj.", "();");
        System.out.println("set = " + set);
    }
}
