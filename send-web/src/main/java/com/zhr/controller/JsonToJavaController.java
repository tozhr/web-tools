package com.zhr.controller;

import com.zhr.common.ResultData;
import com.zhr.tools.code3.entityParameter.JsonToJavaInfo;
import io.github.sharelison.jsontojava.JsonToJava;
import io.github.sharelison.jsontojava.converter.JsonClassResult;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ZHR
 */
@RestController
public class JsonToJavaController {
    /**
     * json to java object
     *
     *  json          json数据
     *  className     类名
     *  useAnnotation 是否使用注解
     *  packageName   包名
     * @return Java class String
     */
    @PostMapping("json2java")
    public ResultData<String> json2java(
             @RequestBody JsonToJavaInfo jsonToJavaInfo
    ) {
        try {
            List<JsonClassResult> results = new JsonToJava().jsonToJava(jsonToJavaInfo.getJson(), jsonToJavaInfo.getClassName(), jsonToJavaInfo.getPackageName(), jsonToJavaInfo.getUseAnnotation());
            if (CollectionUtils.isEmpty(results)) {
                return new ResultData<>("");
            } else {
                StringBuilder sb = new StringBuilder();
                results.forEach(sb::append);
                return new ResultData<>(sb.toString());
            }
        } catch (Exception e) {
            return new ResultData<>(e.getMessage() + "\n");
        }
    }
}
