package com.zhr.controller;

import com.zhr.common.ResultData;
import com.zhr.tools.code3.CreateClassCode;
import com.zhr.tools.code3.entityParameter.EsbExcelInfo;
import org.springframework.web.bind.annotation.*;

/**
 * @author ZHR
 */
@RestController
public class EsbExcelToJavaController {

    /**
     * ESB Excel文档生成类的文本
     *
     *  className    类名
     *  classMemo    类注释
     *  classAuthor  作者
     *  classData    Excel复制过来的字段
     *  classPackage 包路径
     * @return 生成类的文本
     */
    @PostMapping("esb-excel-2-java")
    public ResultData<String> sendRequest(
            @RequestBody EsbExcelInfo esbInfo) {
        try {
            String classStr = CreateClassCode.createClassStr(esbInfo.getClassName(), esbInfo.getClassMemo(), esbInfo.getClassAuthor(), esbInfo.getClassData(), esbInfo.getClassPackage(), esbInfo.getUseLombok());
            return new ResultData<>(classStr);
        } catch (Exception e) {
            return new ResultData<>(e.getMessage() + "\n");
        }
    }
}
