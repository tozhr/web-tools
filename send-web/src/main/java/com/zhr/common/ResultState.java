package com.zhr.common;

/**
 * 结果状态枚举类
 *
 * @author zhr
 */
public enum ResultState {
    /**
     * 成功
     */
    SUCCESS("000000", "OK"),
    /**
     * 业务执行失败，提示用户
     */
    BIZ_FAIL("100000", "INFO"),
    /**
     * 提示开发者
     */
    NON_DATA("300000", "NON_DATA"),
    /**
     * 未知错误
     */
    SYS_ERROR("900000", "ERROR"),
    /**
     * 需要登录
     */
    LOGIN_REQUIRED("400000", "LOGIN_REQUIRED");
    /**
     * 状态
     */
    private String state;
    /**
     * 状态信息
     */
    private String message;

    ResultState(String state, String message) {
        this.state = state;
        this.message = message;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
