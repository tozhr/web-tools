package com.zhr.common;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 通用数据类 用于服务端与客户端传输数据
 *
 * @author zhr
 */
@Data
@NoArgsConstructor
public class ResultData<T> implements Serializable {
    /**
     * 默认结果
     */
    public static final ResultData SUCCESS = new ResultData<>(null, ResultState.SUCCESS);
    /**
     * 返回数据
     */
    private T data;
    /**
     * 状态码
     */
    private String state;
    /**
     * 状态信息
     */
    private String message;

    public ResultData(T data) {
        this.data = data;
        this.state = ResultState.SUCCESS.getState();
        this.message = ResultState.SUCCESS.getMessage();
    }

    public ResultData(ResultState state) {
        this.state = state.getState();
        this.message = state.getMessage();
    }

    public ResultData(T data, ResultState state) {
        this.state = state.getState();
        this.message = state.getMessage();
        this.data = data;
    }

    public ResultData(T data, String state) {
        this.data = data;
        this.state = state;
    }

    public ResultData(ResultState state, String message) {
        this.state = state.getState();
        this.message = message;
    }

    public ResultData(T data, ResultState state, String message) {
        this.data = data;
        this.state = state.getState();
        this.message = message;
    }
}
