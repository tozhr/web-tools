package com.zhr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ZHR
 */
@SpringBootApplication
public class SendWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(SendWebApplication.class, args);
    }
}
