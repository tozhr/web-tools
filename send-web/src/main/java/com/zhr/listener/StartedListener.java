package com.zhr.listener;

import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class StartedListener implements ApplicationListener<ApplicationStartedEvent> {
    /**
     * 启动成功后打开浏览器指定网址
     *
     * @param applicationStartedEvent 事件类型
     */
    @Override
    public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {
        String url = " http://localhost:8500/";
        String cmd = "rundll32 url.dll,FileProtocolHandler  " + url;
        try {
            Runtime.getRuntime().exec(cmd);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
