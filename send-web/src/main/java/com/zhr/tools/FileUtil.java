package com.zhr.tools;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class FileUtil {
    public static void saveFileString(String content, File file) throws IOException {
        if (!file.exists()) {
            boolean flag = file.createNewFile();
            if (!flag) {
                System.err.println("创建文件夹失败");
            }
        }
        PrintWriter printWriter = new PrintWriter(file, StandardCharsets.UTF_8.displayName());
        printWriter.print(content);
        printWriter.flush();
        printWriter.close();
    }

    public static String getFileString(File file) throws IOException {
        if (!file.exists()) {
            boolean flag = file.createNewFile();
            if (!flag) {
                System.err.println("创建文件夹失败");
            }
        }
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String temp;
        StringBuilder sb = new StringBuilder();
        while ((temp = reader.readLine()) != null) {
            sb.append(temp);
        }
        return sb.toString();
    }
}
