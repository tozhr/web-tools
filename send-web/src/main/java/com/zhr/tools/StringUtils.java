package com.zhr.tools;

import cn.hutool.core.util.StrUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author ZHR
 * @date 2019/11/16
 */

public class StringUtils {
    public static String shuffle(String string) {
        List<Character> characters = new ArrayList<>();
        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            characters.add(c);
        }
        Collections.shuffle(characters);
        StringBuilder sb = new StringBuilder();
        characters.forEach(sb::append);
        return sb.toString();
    }

    public static String upperFirstLatter(String str) {
        if (StrUtil.isEmpty(str)) {
            return "";
        } else {
            return Character.toUpperCase(str.charAt(0)) + str.substring(1);
        }
    }

    public static String lowerFirstLatter(String str) {
        if (StrUtil.isEmpty(str)) {
            return "";
        } else {
            return Character.toLowerCase(str.charAt(0)) + str.substring(1);
        }
    }
}
