package com.zhr.tools.code3.entityParameter;

import lombok.Data;

@Data
public class EsbExcelInfo {

    private String className;
    private String classMemo;
    private String classAuthor;
    private String classPackage;
    private String classData;
    private Boolean useLombok;


}
