package com.zhr.tools.code3.entityParameter;

import lombok.Data;

@Data
public class JsonToJavaInfo {
    private String json;
    private String className;
    private Boolean useAnnotation;
    private String packageName;


}
