package com.zhr.tools.code3.scanner;



import com.zhr.tools.code3.entity.ClassFieldInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author a
 */
public class ClassFieldInfoScanner {
    /**
     * @param filePath excel粘贴的字段的文件位置
     * @return 实体类信息
     * @throws IOException \
     */
    public static List<ClassFieldInfo> scanClassFieldInfo(String filePath) throws IOException {
        File excelFile = new File(filePath);
        Scanner scanner = new Scanner(new FileInputStream(excelFile));
        return getClassFieldInfos(scanner);
    }

    /**
     * @param data excel粘贴的字段的文件位置
     * @return 实体类信息
     */
    public static List<ClassFieldInfo> scanClassFieldInfoFromString(String data) {
        Scanner scanner = new Scanner(data);
        return getClassFieldInfos(scanner);
    }

    private static List<ClassFieldInfo> getClassFieldInfos(Scanner scanner) {
        List<ClassFieldInfo> infos = new ArrayList<>();
        ClassFieldInfo info;
        while (scanner.hasNext()) {
            String nextLine = scanner.nextLine();
            Scanner ssScanner = new Scanner(nextLine);
            String originName = ssScanner.next();
            StringBuilder memoSb = new StringBuilder();
            memoSb.append(ssScanner.next());
            String type = ssScanner.next();
            while (ssScanner.hasNext()) {
                memoSb.append(" ").append(ssScanner.next());
            }
            info = new ClassFieldInfo(originName, memoSb.toString(), type);
            infos.add(info);
        }
        scanner.close();
        return infos;
    }
}
