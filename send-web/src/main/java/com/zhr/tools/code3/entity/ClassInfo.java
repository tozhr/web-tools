package com.zhr.tools.code3.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;

/**
 * @author a
 */
@Data
@Accessors(chain = true)
public class ClassInfo {
    private List<ClassFieldInfo> fields;
    private String name;
    private String memo;
    private String author;
    private Date createDate;
    private boolean useLombok = true;
    private boolean useChain = true;
    private Set<String> imports = new HashSet<>(32);
    private String packagePath;

    public void setFields(List<ClassFieldInfo> fields) {
        if (fields == null) {
            fields = new ArrayList<>();
        }
        this.fields = fields;
        fields.forEach(e -> {
            if ("BigDecimal".equals(e.getType())) {
                imports.add("import java.math.BigDecimal;\n");
            }
        });
    }
}
