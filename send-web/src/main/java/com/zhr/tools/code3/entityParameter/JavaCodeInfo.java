package com.zhr.tools.code3.entityParameter;


import lombok.Data;

@Data
public class JavaCodeInfo {

    private String javaCode;
    private String methodPrefix;
    private String genCodePrefix;
    private String genCodeSuffix;


}
