package com.zhr.tools.code3.entityParameter;

import lombok.Data;

@Data
public class AesInfo {

    /**
     * 秘钥
     */
    private String key;
    /**
     * 1 加密 2 解密
     */
    private int type;
    /**
     * 待加解密内容
     */
    private String content;


}
