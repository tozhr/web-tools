package com.zhr.tools.code3.entityParameter;

import lombok.Data;

@Data
public class EsbInfo {
    private String ip;
    private int port;
    private String xml;


}
