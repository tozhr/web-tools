package com.zhr.tools.code3.entity;

import com.zhr.tools.StringUtils;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ZHR
 */
@Data
public class ClassFieldInfo {
    /**
     * 类型映射
     */
    public static final Map<String, String> JAVA_TYPE_MAP = new HashMap<>(16);

    static {
        // 字符串
        JAVA_TYPE_MAP.put("DEFAULT", "String");
        JAVA_TYPE_MAP.put("字符", "String");
        JAVA_TYPE_MAP.put("日期", "String");
        JAVA_TYPE_MAP.put("varchar", "String");
        JAVA_TYPE_MAP.put("varchar2", "String");
        JAVA_TYPE_MAP.put("string", "String");

        // 整数
        JAVA_TYPE_MAP.put("integer", "int");
        JAVA_TYPE_MAP.put("整数", "int");

        // 小数
        JAVA_TYPE_MAP.put("Number", "BigDecimal");
        JAVA_TYPE_MAP.put("金额", "BigDecimal");
        JAVA_TYPE_MAP.put("double", "BigDecimal");
    }

    /**
     * 原始字段名
     */
    private String originName;
    /**
     * 变量名
     */
    private String variableName;
    /**
     * 变量名 首字母大写
     */
    private String variableNameUpperFirst;
    /**
     * 注释
     */
    private String memo;
    /**
     * java 变量类型
     */
    private String type;

    public ClassFieldInfo(String originName, String memo, String type) {
        this.originName = originName;
        this.variableName = StringUtils.lowerFirstLatter(originName);
        this.variableNameUpperFirst = StringUtils.upperFirstLatter(originName);
        this.memo = memo;
        this.type = JAVA_TYPE_MAP.get("DEFAULT");
        for (Map.Entry<String, String> entry : JAVA_TYPE_MAP.entrySet()) {
            if (type.toLowerCase().contains(entry.getKey())) {
                this.type = entry.getValue();
                break;
            }
        }
    }
}
