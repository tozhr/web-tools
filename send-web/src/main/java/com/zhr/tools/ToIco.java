package com.zhr.tools;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.util.ThumbnailatorUtils;
import net.sf.image4j.codec.ico.ICOEncoder;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.List;

public class ToIco {
    public static ByteArrayOutputStream toIco(int size, String format, InputStream imageInputStream) throws IOException {
        BufferedImage bufferedImage = Thumbnails.of(imageInputStream)
                //.sourceRegion(0, 0, 40, 450)
                .size(size, size)
                .keepAspectRatio(false)
                .outputFormat(format)
                .asBufferedImage();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ICOEncoder.write(bufferedImage, byteArrayOutputStream);
        return byteArrayOutputStream;
    }

    public static List<String> getSupportedImageFormats() {
        return ThumbnailatorUtils.getSupportedOutputFormats();
    }

    public static boolean isSupported(String format){
       return ThumbnailatorUtils.isSupportedOutputFormat(format);
    }
}
